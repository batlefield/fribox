window.addEventListener('load', function() {
	//stran nalozena
	
	var prizgiCakanje = function() {
		document.querySelector(".loading").style.display = "block";
	}
	
	var ugasniCakanje = function() {
		document.querySelector(".loading").style.display = "none";
	}
	
	document.querySelector("#nalozi").addEventListener("click", prizgiCakanje);
	
	//Pridobi seznam datotek
	var pridobiSeznamDatotek = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.open("GET", "/datoteke");
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				var datoteke = JSON.parse(xhttp.responseText);
				
				var datotekeHTML = document.querySelector("#datoteke");
				
				for (var i=0; i<datoteke.length; i++) {
					var datoteka = datoteke[i];
					
					var velikost = datoteka.velikost;
					var enota = "B";
					
					if(velikost < 1024) {
						velikost = velikost;
						enota = "B";
					} else if (velikost < 1024 * 1024) {
						velikost = velikost / 1024;
						enota = "KiB";
					} else if (velikost < 1024 * 1024 * 1024) {
						velikost = velikost / 1024 / 1024;
						enota = "MiB";
					} else if (velikost < 1024 * 1024 * 1024 * 1024) {
						velikost = velikost / 1024 / 1024 / 1024;
						enota = "GiB";
					}
					
					velikost = velikost.toFixed(2);
					
					datotekeHTML.innerHTML += " \
						<div class='datoteka senca rob'> \
							<div class='naziv_datoteke'> " + datoteka.datoteka + "  (" + velikost + " " + enota + ") </div> \
							<div class='akcije'> \
							| <span><a target='_blank' href='/poglej/" + datoteka.datoteka + "' target='_self'>Poglej</a></span> \
							| <span><a href='/prenesi/" + datoteka.datoteka + "' target='_self'>Prenesi</a></span> \
							| <span akcija='brisi' datoteka='"+ datoteka.datoteka +"'>Izbriši</span> </div> \
					    </div>";	
				}
				
				var spans = document.querySelectorAll("span[akcija=brisi]");
				
				for(var i = 0; i < spans.length; i++) {
					spans[i].addEventListener("click", brisi);
				}
				/*
				if (datoteke.length > 0) {
					document.querySelector("span[akcija=brisi]").addEventListener("click", brisi);
				}*/
				ugasniCakanje();
			}
		};
		xhttp.send();
	}
	
	var brisi = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (xhttp.responseText == "Datoteka izbrisana") {
					window.location = "/";
				} else {
					alert("Datoteke ni bilo možno izbrisati!");
				}
			}
			ugasniCakanje();
		};
		xhttp.open("GET", "/brisi/"+this.getAttribute("datoteka"), true);
		xhttp.send();
	}
	
	pridobiSeznamDatotek();

});